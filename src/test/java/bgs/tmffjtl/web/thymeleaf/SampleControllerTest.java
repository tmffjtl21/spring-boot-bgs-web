package bgs.tmffjtl.web.thymeleaf;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlHeading1;
import com.gargoylesoftware.htmlunit.html.HtmlPage;

import bgs.tmffjtl.web.thymeleaf.SimpleController;

@RunWith(SpringRunner.class)
@WebMvcTest(SimpleController.class)
public class SampleControllerTest {
	
//	@Autowired
//	MockMvc mockMvc;			
	
	// htmlunit을 사용하면 좀더 html에 특화된 테스트코드를 작성할 수 있다. 
	@Autowired
	WebClient webClient;
	
	@Test
	public void hello() throws Exception {
		
		// 요청 : "/hello"
		// 응답 
		//	- 모델 name : taejin
		// - 뷰 이름 : hello
//		mockMvc.perform(get("/hello"))
//						.andExpect(status().isOk())
//						.andDo(print())
//						.andExpect(view().name("hello"))
//						.andExpect(model().attribute("name", is("taejin")))
//						.andExpect(content().string(containsString("taejin")));
		
		// htmlunit을 사용한 html테스트코드 작성
		HtmlPage page = webClient.getPage("/hello");
		HtmlHeading1 h1 = page.getFirstByXPath("//h1");
		assertThat(h1.getTextContent()).isEqualToIgnoringCase("taejin");
		
	}
}
