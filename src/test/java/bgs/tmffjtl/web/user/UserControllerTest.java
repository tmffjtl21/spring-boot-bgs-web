package bgs.tmffjtl.web.user;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {
	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void hello() throws Exception {
		mockMvc.perform(get("/helloori"))
			.andExpect(status().isOk())
			.andExpect(content().string("hello"));
	}
	
	@Test
	public void createUser_JSON() throws Exception {
		
		String userJson = "{\"username\":\"taejin\", \"password\":\"123\"}";
		mockMvc.perform(post("/users/create")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.accept(MediaType.APPLICATION_JSON_UTF8)
					.content(userJson))
//					.andDo(print())
				.andExpect(status().isOk())
				.andExpect(jsonPath("$.username", is(equalTo("taejin"))))
				.andExpect(jsonPath("$.password", is(equalTo("123"))));
	}
	
	// ContentNegotiatingViewResolver (type설정에 따라 응답 결과를 다르게 처리하도록 도와주는 역할)
	// 가 이미 등록돼 있다는 가정하에 요청을 테스트 
	// pom.xml에 아래 dependency 추가
//	<dependency>
//		<groupId>com.fasterxml.jackson.dataformat</groupId>
//		<artifactId>jackson-dataformat-xml</artifactId>
//		<scope>2.9.6</scope>
//	</dependency>
	@Test
	public void createUser_XML() throws Exception {
		
		String userJson = "{\"username\":\"taejin\", \"password\":\"123\"}";
		mockMvc.perform(post("/users/create")
					.contentType(MediaType.APPLICATION_JSON_UTF8)
					.accept(MediaType.APPLICATION_XML)
					.content(userJson))
//					.andDo(print())
				.andExpect(status().isOk())
				.andExpect(xpath("/User/username").string("taejin"))
				.andExpect(xpath("/User/password").string("123"));
	}
}
