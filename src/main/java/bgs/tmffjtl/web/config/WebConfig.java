package bgs.tmffjtl.web.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**추가적인 Web 설정을 만들기 위한 클래스*/
@Configuration
//@EnableWebMvc		// 이걸 임포트 하면 스프링 MVC를 재정의 한다. 없으면 확장하는 개념
public class WebConfig implements WebMvcConfigurer{
	
	// 리소스 핸들러 추가 작업
	// 대신 캐싱전략을 따로 적용해줘야된다. 스프링 기본설정으로 안해줌..
	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/m/**")
			.addResourceLocations("classpath:/m/")
			.setCachePeriod(20);
	}
	
	// CORS 설정 추가하기
	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/helloCors")
				.allowedOrigins("http://127.0.0.1:18080");
	}
}
