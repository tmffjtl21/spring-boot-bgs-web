package bgs.tmffjtl.web.hateoas;

import lombok.Data;

@Data
public class HateoasSample {
	
	private String prefix;
	private String name;
}
