package bgs.tmffjtl.web.hateoas;

import org.springframework.hateoas.Resource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
public class HateoasController {
	
	@GetMapping("/helloHateoas")
	public Resource<HateoasSample> hello() {
		
		HateoasSample hateoasSample = new HateoasSample();
		hateoasSample.setPrefix("Hey, ");
		hateoasSample.setName("taejin");
		
		Resource<HateoasSample> hateoasResource = new Resource<>(hateoasSample);
		// link를 self 라는 릴레이션으로 만들어서 추가.. static 임포트가 이클립스에선 안돼서 수동으로 해줘야함;
		hateoasResource.add(linkTo(methodOn(HateoasController.class).hello()).withSelfRel());
		
		return hateoasResource;
	}
}
