package bgs.tmffjtl.web.exception;

import lombok.Data;

// 이 앱의 특화되어있는 에러정보를 갖고 있는 클래스

@Data
public class AppError {
	private String message;
	private String reason;
}
