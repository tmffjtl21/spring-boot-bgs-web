package bgs.tmffjtl.web.exception;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice		// 당신의 스프링 어플리케이션에게 이 클래스가 당신의 어플리케이션의 예외처리를 맡을 거라고 알려주게 된다.
@Controller
public class ExceptionController {
	
	@GetMapping("/helloException")
	public String hello() {
		throw new SampleException();
	}
	
	// 에러를 처리하는 ExceptionHandler
	@ExceptionHandler(SampleException.class)
	public @ResponseBody AppError sampleError(SampleException e) {
		AppError appError = new AppError();
		appError.setMessage("error.app.key");
		appError.setReason("IDK IDK IDK");
		return appError;
	}
	
}
