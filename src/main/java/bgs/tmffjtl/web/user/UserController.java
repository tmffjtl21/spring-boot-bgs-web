package bgs.tmffjtl.web.user;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import bgs.tmffjtl.web.exception.SampleException;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class UserController {
	
	@GetMapping("/helloori")
	public String hello() {

		return "hello";
	}
	
	@PostMapping("/users/create")
	public User create(@RequestBody User user) {	// RestController를 사용하면 @ResponseBody 를 생략 가능 
		
		log.debug(user.toString());
		return user;
	}
}
