package bgs.tmffjtl.web.cors;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CorsController {
	
//	@CrossOrigin(origins="http://127.0.0.1:18080")		// 이렇게 해도 되고 WebConfig파일에 따로 설정으로 뺄 수 있다.
	@GetMapping("/helloCors")
	public String helloCors() {
		return "helloCors";
	}
}
