package bgs.tmffjtl.web.cors;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CorsSpringApplication {
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(CorsController.class);
		app.run(args);
	}
}
