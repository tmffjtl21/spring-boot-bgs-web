package bgs.tmffjtl.web.thymeleaf;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SimpleController {
	
	@GetMapping("/hello")
	public String hello(Model model) {		// 예전엔 ModelAndView라는걸 많이 썼는데 Model만 써도 된다. 
		model.addAttribute("name","taejin");
		return "hello";		// 이제 리턴하는 것은 ResponseBody가 아님 / RestController가 아니라 그냥 Controller 
	}
}
